#!/bin/bash

while true; do
  time=$(date '+%Y%m%d%H%M%S')
  cd proj2
  echo $time > test123
  git add .
  git commit -m "Update proj2 $time"
  git push
  cd -
  cd proj3
  echo $time > test123
  git add .
  git commit -m "Update proj3 $time"
  git push
  cd -
  git add .
  git commit -m "Update submodules $time"
  git push
  echo "WAIT 30s"
  sleep 30
done